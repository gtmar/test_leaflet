<?php
/**
 * This class is used for connecting to the database and to handle all the queries made by the visualization module
 *
 * \author 	Ayush Shrestha <ashrestha@airsage.net>
 * \date	10/14/2014
 */

class Toolkit {
	public $table;
	public $database;
	public $user;
	public $password;
	public $port;
	public $host;

	private $dbconn;

	private function logQuery($message) {
		$logFile = "query.log";
		file_put_contents($logFile, "\n" .date("Y-m-d H:i:s")."\t".$message, FILE_APPEND);
	}

	public function __construct($array) {
		if (!empty($array)) {

			if ($array["table"] != "")
				$this -> table = $array["table"];
			if ($array["database"] != "")
				$this -> database = $array["database"];
			if ($array["port"] != "")
				$this -> port = $array["port"];
			if ($array["user"] != "")
				$this -> user = $array["user"];
			if ($array["password"] != NULL && $array["password"] != "")
				$this -> password = $array["password"];
			if ($array["host"] != "")
				$this -> host = $array["host"];

			$this -> dbconn = pg_connect("host=" . $this -> host . " port=" . $this -> port . " dbname=" . $this -> database . " user=" . $this -> user. " password=".$this->password) or die('Could not connect to database.');
		}
	}

	public function getConditionString($array) {
		$dirty = false;
		$tempString = "";

		if (!empty($array)) {
			if ($array["table"] != "") {
				$this -> table = $array["table"];
			}
			if ($array["user"] != "") {
				$this -> user = $array["user"];
			}
			if ($array["password"] != "") {
				$this -> password = $array["password"];
			}
			if ($array["database"] != "") {
				$this -> database = $array["database"];
			}

			if ($array["pointType"] != "" && $array["pointType"] != "A") {
				$tempString .= " where ";
				$tempString .= "point_type = '" . $array['pointType'] . "'";
				$dirty = true;
			}
			if ($array['startEpoch'] != -1) {
				$tempString .= $dirty ? " and " : " where ";
				$tempString .= "initial_epoch_time > " . $array['startEpoch'];
				$dirty = true;
			}
			if ($array['endEpoch'] != -1) {
				$tempString .= $dirty ? " and " : " where ";
				$tempString .= "initial_epoch_time < " . $array['endEpoch'];
				$dirty = true;
			}
			if ($array['stateCode'] != -1) {
				$tempString .= $dirty ? " and " : " where ";
				$tempString .= "substring(blockid,1,2) like '" . $array['stateCode'] . "'";
				$dirty = true;
			}
			if ($array['fromDuration'] != -1) {
				$tempString .= $dirty ? " and " : " where ";
				$tempString .= "point_duration > " . $array['fromDuration'] . "*60";
				$dirty = true;
			}
			if ($array['toDuration'] != -1) {
				$tempString .= $dirty ? " and " : " where ";
				$tempString .= "point_duration < " . $array['toDuration'] . "*60";
				$dirty = true;
			}
			if ($array['selJson'] != "") {
				$tempString .= $dirty ? " and " : " where ";
				$qstr = "SRID=4326;LINESTRING (";

				$j = json_decode(str_replace('\\','',$array['selJson']));

				for ($i = 0; $i < count($j -> geometry -> coordinates[0]); $i++)
					$qstr .= $j -> geometry -> coordinates[0][$i][0] . " " . $j -> geometry -> coordinates[0][$i][1] . ",";

				$qstr = substr($qstr, 0, count($qstr) - 2);
				$qstr .= ")";

				$tempString .= "ST_Intersects(ST_MakePolygon(ST_GeomFromEWKT('" . $qstr . "')), latlng)";
				$dirty = true;
            }
            if ($array['call'] != "" && $array['call'] != "A") {
                $tempString .= $dirty ? " and " : " where ";
                switch($array['call']){
                case 'P':
                    $tempString .= "dialed_911 > 0";
                    break;
                case 'T':
                    $tempString .= "dialed_511 > 0";
                    break;
                case 'O':
                    $tempString .= "dialed_other > 0";
                    break;
                default:
                    break;
                }
				$dirty = true;
			}
		}

		return $tempString;
	}

	public function executeQuery($query) {
		$this->logQuery($query);
		return pg_fetch_all(pg_query($query));
	}
	

	public function getCSV($query) {
		$result = pg_query($query) or die('Couldn\'t fetch records');
		$num_fields;
		
		if ($result) {
			$num_fields = pg_num_fields($result);
			$headers = array();
			for ($i = 0; $i < $num_fields; $i++) {
				$headers[] = pg_field_name($result, $i);
			}
		}

		$fp = fopen('php://output', 'w');
		if ($fp && $result) {
			header('Content-Type: text/csv');
			header('Content-Disposition: attachment; filename="export.csv"');
			header('Pragma: no-cache');
			header('Expires: 0');
			fputcsv($fp, $headers);
			while ($row = pg_fetch_array($result)) {
				$temparr = array();
				for($i=0;$i<$num_fields;$i++){
					array_push($temparr, $row[$i]);					
				}
				fputcsv($fp, $temparr);
			}
			die ;
		}
	}

	public function __destruct() {
		pg_close($this -> dbconn);
	}

}
?>
