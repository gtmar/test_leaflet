google.load("visualization", "1", {
    packages : ["corechart"]
});

var M ={};
M.map = L.map('map', {
    zoomControl : true,
}).setView([18.200178, -66.664513], 8);

//L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpandmbXliNDBjZWd2M2x6bDk3c2ZtOTkifQ._QA7i5Mpkd_m30IGElHziw', {
L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    //maxZoom: 8,
    attribution: '<a href="http://www.airsage.com">Airsage &copy;</a>',
    //id: 'mapbox.streets'
}).addTo(M.map);

M.draw = new L.Control.Draw({
    draw : {
        polyline : false,
        marker : false,
        circle : false
    }
});
M.map.on('draw:created', function(e) {
    T.set('vmSelJson', JSON.stringify(e.layer.toGeoJSON()));
    performDBQuery();
});
M.map.addControl(M.draw);

L.easyButton('fa-bar-chart', function (){ 
    //$(".chartbox").toggle();
        $(".chartbox").animate({width: 'toggle'}, 250);
}, 'Show Charts').addTo(M.map);

L.easyButton('fa-filter', function (){ 
    //$(".filterbox").toggle();
        $(".filterbox").animate({height: 'toggle'}, 250);
}, 'Show Filters').addTo(M.map);

L.easyButton('fa-file-zip-o', function(){
    $('#shapefile').click();
}, 'Add Custom Shapefile').addTo(M.map);



M.charts = L.control({ position: 'topright'});
M.charts.onAdd = function (map) {
    this._div = L.DomUtil.create('div', 'chartbox'); 
    this.update();
    return this._div;
};
M.charts.update = function(props){
    this._div.innerHTML = '<div><button id="btnCharts" class="k-button">Refresh</button></div><hr /><div class="container-fluid" id="charts"></div>';
}
M.charts.addTo(M.map);

M.filter = L.control({ position: 'topleft'});
M.filter.onAdd = function (map) {
    this._div = L.DomUtil.create('div', 'filterbox'); 
    this.update();
    return this._div;
};

M.filter.update = function (props) {
    this._div.innerHTML = '<div id="toolbox"><div><label for="actPointType">Point Type:</label><input id="actPointType" data-bind="value: vmPointType"/></div><div><label for="actFromDatePicker">From:</label><input id="actFromDatePicker" data-bind="value: vmFromDate"/></div><div><label for="actToDatePicker">To:</label><input id="actToDatePicker" data-bind="value: vmToDate"/></div><div><label for="actDurationFrom">Longer than (min):</label><input id="actDurationFrom"  class="k-textbox" data-bind="value: vmDurationFrom"/></div><div><label for="actDurationTo">Shorter than (min):</label><input id="actDurationTo" class="k-textbox" data-bind="value: vmDurationTo"/></div><div><label for="actCertainty">Certainty radius:</label><input id="actCertainty" class="k-textbox" data-bind="value: vmCertainty"/></div><div><label for="actCall">Call Type:</label><input id="actCall" data-bind="value: vmCall"/></div></div><div class="operators"><button id="btnFilter" class="k-button">Filter</button><button id="btnReset" class="k-button">Reset</button></div>'
/*
    this._div.innerHTML = '<div class="container" id="toolbox">'+
            '<div class="row no-gutter">'+
                '<label class="col-lg-1" for="actPointType">Point Type: </label>'+
                '<div class="col-lg-3"><input id="actPointType" data-bind="value: vmPointType"/></div>'+
            '</div><div class="row no-gutter">'+
                '<label class="col-lg-2" for="actFromDatePicker">From:</label>'+
                '<label class="col-lg-2" for="actToDatePicker">To:</label>'+
            '</div><div class="row no-gutter">'+
                '<div class="col-lg-2"><input id="actFromDatePicker" data-bind="value: vmFromDate"/></div>'+
                '<div class="col-lg-2"><input id="actToDatePicker" data-bind="value: vmToDate"/></div>'+
            '</div><div class="row no-gutter">'+
                '<label class="col-lg-2" for="actDurationFrom">Longer than (min):</label>'+
                '<label class="col-lg-2" for="actDurationTo">Shorter than (min):</label>'+
            '</div><div class="row no-gutter">'+
                '<div class="col-lg-2"><input id="actDurationTo" class="k-textbox" data-bind="value: vmDurationTo"/></div>'+
                '<div class="col-lg-2"><input id="actDurationFrom"  class="k-textbox" data-bind="value: vmDurationFrom"/></div>'+
            '</div><div class="row no-gutter">'+
                '<div><label class="col-lg-1" for="actCertainty">Certainty radius:</label><input id="actCertainty" class="k-textbox" data-bind="value: vmCertainty"/></div>'+
            '</div>'+
        '</div>'+
        '<div class="operators"><button id="btnFilter" class="k-button">Filter</button><button id="btnReset" class="k-button">Reset</button></div>';
        */
};

M.filter.addTo(M.map);

M.search = new L.esri.Controls.Geosearch().addTo(M.map);
M.search.on('results', function(d) {
    M.map.fitBounds(d.bounds);
});

var dbNotify= $("#notification").kendoNotification({
    stacking : "down",
    show : showNotification,
    button : true
}).data("kendoNotification");

function showNotification(e) {
    if (!$("." + e.sender._guid)[1]) {
        var element = e.element.parent(), eWidth = element.width(), eHeight = element.height(), wWidth = $(window).width(), wHeight = $(window).height(), newTop, newLeft;

        newLeft = Math.floor(wWidth / 2 - eWidth / 2);
        newTop = Math.floor(wHeight / 2 - eHeight / 2);

        e.element.parent().css({
            top : newTop,
            left : newLeft
        });
    }
}


function notify(m){
    dbNotify.show("Error: "+m);
    $("#loader").fadeOut();
}


function reset(){
    T.set('vmPointType', T_CONST.PointType);
    T.set('vmDurationFrom',T_CONST.DurationFrom);
    T.set('vmDurationTo',T_CONST.DurationTo);
    T.set('vmCertainty',T_CONST.Certainty);
    T.set('vmFromDate',T_CONST.FromDate);
    T.set('vmToDate',T_CONST.ToDate);
    T.set('vmCall',T_CONST.Call);
    performDBQuery();
}

function performDBQuery(){
    $("#loader").show();
    var query = "fetchvals.php?" + constructQuery();
    console.log(query);
    $.ajax({
        url: query,
        complete: render,
        error: notify
    });
}

function plotAP(qr){
    var markers = M.map.getZoom() > 9? new L.FeatureGroup(): new L.MarkerClusterGroup();
    markers.id = "QueryClusterAP";
    for(var i=0; i < qr.length; i++){
        markers.addLayer(new L.Circle(new L.LatLng(qr[i].latitude, qr[i].longitude), 5));
    }

    if (markers.options) markers.options.spiderfyOnMaxZoom = false;
    M.map.eachLayer(function(layer) {
        if (layer.id == "QueryClusterAP")
            M.map.removeLayer(layer);
    });
    M.map.addLayer(markers);
    M.map.fitBounds(markers.getBounds());
}

function bothLatLngPresent(c){
    return c.latitude && c.longitude && c.longitude.length > 0 && c.latitude.length > 0;
}

function plotHW(qr){
    var markers = M.map.getZoom() > 9? new L.FeatureGroup(): new L.MarkerClusterGroup();
    markers.id = "QueryClusterHW";
    var m = null;
    for(var i=0; i < qr.length; i++){
        if(bothLatLngPresent(qr[i]))
            m = new L.Circle(new L.LatLng(qr[i].latitude, qr[i].longitude), 10);
            m.setStyle({color: 'red', fillColor: 'red'});
            markers.addLayer(m);
    }

    if (markers.options) markers.options.spiderfyOnMaxZoom = false;
    M.map.eachLayer(function(layer) {
        if (layer.id == "QueryClusterHW")
            M.map.removeLayer(layer);
    });
    M.map.addLayer(markers);
}

function render(r){
    var qr = null;
    try{
        if(r == null) throw "No response obtained";
        qr  = JSON.parse(r.responseText);
        if(qr == null) throw "JSON parse error";
        if(!qr) throw "Zero results returned";
    }
    catch(e){
        notify(e);
        return;
    }
    plotAP(qr.ap);
    plotHW(qr.hw);
    
    $("#loader").fadeOut();
}

function filter(){
    performDBQuery();
}

function drawCharts(r){
    $("#loader").show();
    var qr = JSON.parse(r.responseText);
    if(!qr) return;

    $('#charts').empty();

    $('#charts').append('<div id="charts0" class="row"></div>');

    $('#charts0').append('<div class="col-lg-10" id="chart_stats"><p>Unique devices seen: <strong>'+qr.uemins[0].count+'</strong></p><p>Total number of activties: <strong>'+qr.total[0].count+'</strong></p></div>');

    $('#charts').append('<div id="charts1" class="row"></div>');
    $('#charts1').append('<div class="col-lg-10 xheight" id="chart_time"></div>');
    var chart_time = new google.visualization.ColumnChart(document.getElementById('chart_time'));
    chart_time.draw(getTable([['Time of Day','Activity Points', { role:'style'}]],qr.epochtime, T_CONST.Colors[0]),{title: 'Distribution by day parts', legend:'none'});
    
    $('#charts').append('<div id="charts2" class="row"></div>');
    $('#charts2').append('<div class="xheight" id="chart_duration"></div>');
    var chart_duration = new google.visualization.ColumnChart(document.getElementById('chart_duration'));
    chart_duration.draw(getTable([['Duration','Activity Points', { role:'style'}]],qr.duration, T_CONST.Colors[1]),{title: 'Distribution by duration (mins)', legend:'none'});

    $('#charts').append('<div id="charts3" class="row"></div>');
    $('#charts3').append('<div class="xheight" id="chart_point_type"></div>');
    var chart_point_type = new google.visualization.PieChart(document.getElementById('chart_point_type'));
    chart_point_type.draw(getTable([['Point Type','Percentage']],qr.pointtype),{title:'Distribution by point types', pieHole: 0.4, tooltip: {text: 'percentage'}, slices : {0 : {color : T_CONST.Colors[0]},1 : {color : T_CONST.Colors[1]},2 : {color : T_CONST.Colors[2]},3 : {color : T_CONST.Colors[3]},4 : {color : T_CONST.Colors[4]},5 : {color : T_CONST.Colors[5]},},pieSliceTextStyle : {color : 'black'}});

    $('#charts').append('<div id="charts4" class="row"></div>');
    $('#charts4').append('<div class="xheight" id="chart_activity_type"></div>');
    var chart_activity_type = new google.visualization.PieChart(document.getElementById('chart_activity_type'));
    chart_activity_type.draw(getTable([['Activity Type','Percentage']],qr.acttype),{title:'Distribution by activity types', pieHole: 0.4, tooltip: {text: 'percentage'}, slices : {0 : {color : T_CONST.Colors[0]},1 : {color : T_CONST.Colors[1]},2 : {color : T_CONST.Colors[2]},3 : {color : T_CONST.Colors[3]},4 : {color : T_CONST.Colors[4]},5 : {color : T_CONST.Colors[5]},},pieSliceTextStyle : {color : 'black'} });

    $('#charts').append('<div class="row"" id="charts5"></div>');
    $('#charts5').append('<div class="xheight" id="chart_dialed"></div>');
    var chart_dialed = new google.visualization.PieChart(document.getElementById('chart_dialed'));
    chart_dialed.draw(getTable([['Dialed number Type','Percentage']],qr.dialed),{title:'Distribution by dialed numbers', pieHole: 0.4, tooltip: {text: 'percentage'}});
    $("#loader").fadeOut();
}

function getTable(rows,data,color){
    if(color!=null &&  color!=undefined)
        for(i in data) rows.push([data[i].range, +data[i].count, color]);
    else
        for(i in data) rows.push([data[i].range, +data[i].count]);
    return google.visualization.arrayToDataTable(rows);
}

function queryCharts(){
    var query="fetchstats.php?"+constructQuery();
    console.log(query);
    $.ajax({
        url: query,
        complete: drawCharts,
        error: notify
    });
}

var T_CONST = {
    PointType: "A",
    DurationFrom: "-1",
    DurationTo: "-1",
    Certainty: "-1",
    FromDate: null,
    ToDate: null,
    State: "-1",
    SelJson: null,
    Call: "A",
    Colors : ['#B5E28C', '#FD9C73', '#bc80bd', '#fdb462', '#80b1d3', '#ffd92f']
}

var T = kendo.observable({
    vmPointType: T_CONST.PointType,
    vmDurationFrom: T_CONST.DurationFrom,
    vmDurationTo: T_CONST.DurationTo,
    vmCertainty: T_CONST.Certainty,
    vmFromDate: T_CONST.FromDate,
    vmToDate: T_CONST.ToDate,
    vmStateCode: T_CONST.State,
    vmSelJson: T_CONST.SelJson,
    vmCall: T_CONST.Call
});

function constructQuery(){
    var query = 'pointType=' + T.vmPointType + '&startEpoch=' + convertEpoch(T.vmFromDate) + '&endEpoch=' + convertEpoch(T.vmToDate) +'&fromDuration=' + T.vmDurationFrom+ '&toDuration=' + T.vmDurationTo+ '&certainty=' + T.vmCertainty + '&stateCode=' + T.vmStateCode + '&selJson=' + T.vmSelJson+ '&call='+ T.vmCall;
    return query;
}

function convertEpoch(d){
    return d == null? "-1": new Date(d).getTime() / 1000;
}
$('#actCall').kendoDropDownList({
    dataTextField: "Type",
    dataValueField: "Abbrv",
    dataSource: {
        transport:{
            read:{
                dataType: "json",
                url: "scripts/call-types.json"
            }
        }
    }
});

function add_shapefile(filename){
    var shpfile = new L.Shapefile(filename, {
        style: {
            "color": "#000",
            "opacity": 1.0,
            "weight": 3.0,
            "fillColor": "#FFF",
            "fillOpacity": 0.0,
        },
        isArrayBuffer: true,
        onEachFeature: function(feature, layer) {
            if (feature.properties) {
                layer.bindPopup(Object.keys(feature.properties).map(function(k) {
                    return k + ": " + feature.properties[k];
                }).join("<br />"), {
                    maxHeight: 200
                });
            }
        }
    });
    shpfile.addTo(M.map);
}

$('#actPointType').kendoDropDownList({
    dataTextField: "Type",
    dataValueField: "Abbrv",
    dataSource: {
        transport:{
            read:{
                dataType: "json",
                url: "scripts/data-point-types.json"
            }
        }
    }
});


$('#actFromDatePicker').kendoDatePicker();
$('#actToDatePicker').kendoDatePicker();

$('#btnFilter').kendoButton({
    click: filter 
});

$('#btnReset').kendoButton({
    click: reset
});

$('#btnCharts').kendoButton({
    icon: "refresh",
    click: queryCharts 
});

$(".filterbox").hide();
$(".chartbox").hide();
$("#loader").hide();

$("#shapefile").on('change', function(f){
    var reader = new FileReader();
    reader.onload = function(f){
        add_shapefile(f.target.result);
    };
    reader.readAsArrayBuffer(f.target.files[0]);
});

kendo.bind($("#toolbox"),T);
