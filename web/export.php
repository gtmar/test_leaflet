<?php
require 'Toolkit.php';
$params = array('database' => 'vis', 'table' => 'vis_nwap_puerto_rico_ayush', 'host' => 'localhost', 'user' =>'visualizer', 'port' => '5432', 'password' => 'visualizer');
$tk = new Toolkit($params);

$queryString = "select emin,initial_epoch_time,point_duration,latitude,longitude,certainty_radius,point_type,blockid from " . $tk -> table . $tk -> getConditionString($_GET);
$tk -> getCSV($queryString);
?>
