<?php
require 'Toolkit.php';
$params = array('database' => 'vis', 'table' => 'vis_nwap_puerto_rico_ayush', 'host' => 'localhost', 'user' => 'visualizer', 'port' => '5432', 'password' =>'visualizer');
$tk = new Toolkit($params);

/// Query Strings

$qTotalCount = "select count(*) from " . $tk -> table . $tk -> getConditionString($_GET);
//echo $qTotalCount;

$qUniqueEmins = "select count(distinct emin) from " . $tk -> table . $tk -> getConditionString($_GET);

$qPointType = "select point_type as range, count(*) as count from " . $tk -> table . $tk -> getConditionString($_GET). " group by point_type";

$qEpochTime = "SELECT r.range, count(s.*) FROM vis_epoch_range r 
		LEFT JOIN 
	  	(SELECT EXTRACT (HOUR FROM TIMESTAMP WITH TIME ZONE 'epoch' + initial_epoch_time * INTERVAL '1 second') as hours 
	  		FROM " . $tk -> table . $tk -> getConditionString($_GET) . ") s 
	  		ON s.hours
	  		BETWEEN r.r_min AND r.r_max
	GROUP BY r.range
	ORDER BY r.range";

$qDuration = "SELECT r.range, count(s.*)
	FROM vis_duration_range r
	LEFT JOIN 
	(SELECT point_duration 
		FROM " . $tk -> table . $tk -> getConditionString($_GET) . " ) s 
		ON s.point_duration/60 
		BETWEEN r.r_min AND r.r_max
	GROUP BY r.range
	ORDER BY r.range";

$qDialed = '';
if ($_GET['call'] != "") {
    $qDialed = 'SELECT ';
    switch($_GET['call']){
    case 'P':
        $qDialed .= "sum(dialed_911) as t911, 0 as t511, 0 as tOther FROM ";
        break;
    case 'T':
        $qDialed .= " 0 as t911, sum(dialed_511)  as t511, 0 as tOther FROM ";
        break;
    case 'O':
        $qDialed .= "0 as t911, 0 as t511,sum(dialed_other) as tOther FROM ";
        break;
    default:
        $qDialed .= "sum(dialed_911) as t911, sum(dialed_511) as t511, sum(dialed_other) as tOther FROM ";
    }
    $qDialed .= $tk -> table . $tk -> getConditionString($_GET);
}

$qActivityType = "SELECT sum(call) as tCall, sum(sms) as tSMS, sum(data) as tData FROM ". $tk -> table . $tk -> getConditionString($_GET);

/// Query Executions

$rTotalCount = $tk -> executeQuery($qTotalCount);

$rUniqueEmins = $tk -> executeQuery($qUniqueEmins);

$rEpochTime = $tk -> executeQuery($qEpochTime);

$rDuration = $tk -> executeQuery($qDuration);

$rDialed = format($tk->executeQuery($qDialed));

$rActivityType = format($tk->executeQuery($qActivityType));

$rPointType = $tk->executeQuery($qPointType);

/// Postprocessing

function format($arr){
	$r = array();
	foreach($arr as $elem){
		foreach($elem as $key=>$val){
			array_push($r,array('range' => substr($key,1), 'count' =>$val));
		}
	}
	return $r;
}

echo json_encode(array('uemins' => $rUniqueEmins, 
	'epochtime' => $rEpochTime, 
	'duration' => $rDuration, 
	'total' => $rTotalCount, 
	'dialed'=> $rDialed, 
	'acttype' => $rActivityType, 
	'pointtype' => $rPointType));
?>
