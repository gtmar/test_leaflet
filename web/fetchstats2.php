<?php
require 'Toolkit.php';
$params = array('database' => 'vis', 'table' => 'vis_nwap_puerto_rico_ayush', 'host' => 'localhost', 'user' => 'visualizer', 'port' => '5432', 'password' => 'visualizer');
$tk = new Toolkit($params);

$qTotalCount = "select count(*) from " . $tk -> table . $tk -> getConditionString($_GET);

$qUniqueEmins = "select count(distinct emin) from " . $tk -> table . $tk -> getConditionString($_GET);

$qEpochTime = "SELECT r.range, count(s.*) FROM vis_epoch_range r 
		LEFT JOIN 
	  	(SELECT EXTRACT (HOUR FROM TIMESTAMP WITH TIME ZONE 'epoch' + initial_epoch_time * INTERVAL '1 second') as hours 
	  		FROM " . $tk -> table . $tk -> getConditionString($_GET) . ") s 
	  		ON s.hours
	  		BETWEEN r.r_min AND r.r_max
	GROUP BY r.range
	ORDER BY r.range";

$qDuration = "SELECT r.range, count(s.*)
	FROM vis_duration_range r
	LEFT JOIN 
	(SELECT point_duration 
		FROM " . $tk -> table . $tk -> getConditionString($_GET) . " ) s 
		ON s.point_duration/60 
		BETWEEN r.r_min AND r.r_max
	GROUP BY r.range
	ORDER BY r.range";

$rTotalCount = $tk -> executeQuery($qTotalCount);

$rUniqueEmins = $tk -> executeQuery($qUniqueEmins);

$rEpochTime = $tk -> executeQuery($qEpochTime);

$rDuration = $tk -> executeQuery($qDuration);

echo json_encode(array('uemins' => $rUniqueEmins, 'epochtime' => $rEpochTime, 'duration' => $rDuration, 'total' => $rTotalCount));
?>
