<?php
require 'Toolkit.php';
$params = array('database' => 'vis', 'table' => 'vis_nwap_puerto_rico_ayush', 'host' => 'localhost', 'user' => 'visualizer', 'port' => '5432', 'password' => 'visualizer');
$tk = new Toolkit($params);

$qType = "select point_type as type, count(*) as count from " . $tk -> table . $tk -> getConditionString($_GET). " group by point_type";

$qTotalCount = "select count(*) from " . $tk -> table . $tk -> getConditionString($_GET);

$rTotalCount = $tk -> executeQuery($qTotalCount);

$rType = $tk -> executeQuery($qType);

echo json_encode(array('count' => $rTotalCount, 'type' => $rType));
?>
