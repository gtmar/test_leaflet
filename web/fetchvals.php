<?php
require 'Toolkit.php';
$params = array('database' => 'vis', 'table' => 'vis_nwap_puerto_rico_ayush', 'host' => 'localhost', 'user' => 'visualizer', 'port' => '5432', 'password' => 'visualizer');
$tk = new Toolkit($params);
$query = "select st_y(latlng) as latitude, st_x(latlng) as longitude from " . $tk -> table . $tk -> getConditionString($_GET);
$hwquery = "select distinct hw.home_lat as latitude, hw.home_long as longitude from " . $tk -> table . " ap join nwap hw on ap.emin = hw.subscriber_id ". $tk -> getConditionString($_GET) ;
//echo $query;
echo json_encode(array("ap"=> $tk -> executeQuery($query), "hw" => $tk -> executeQuery($hwquery)));
?>
