/**
 * @author  Ayush Shrestha
 * @email   ashrestha@airsage.com
 * @date    Dec 1, 2014
 */

var QueryBuilder = function() {
    // assign the default values for the database connection
    var defhost = "localhost", defuser = "visualizer", defpassword = "visualizer", deftable = "vis_nwap", defdb = "vis";

    var db = {
        host : defhost,
        user : defuser,
        password : defpassword,
        table : deftable,
        database : defdb
    };

    var query = {
        pointType : "",
        startEpoch : -1,
        endEpoch : -1,
        fromDuration : -1,
        toDuration : -1,
        certainty : -1,
        stateCode : -1,
        selectionJSON : ""
    };

    function construct() {
        var queryBaseHttp = "?";

        queryBaseHttp += 'pointType=' + query.pointType;
        queryBaseHttp += '&startEpoch=' + query.startEpoch;
        queryBaseHttp += '&endEpoch=' + query.endEpoch;

        queryBaseHttp += '&fromDuration=' + query.fromDuration;
        queryBaseHttp += '&toDuration=' + query.toDuration;
        queryBaseHttp += '&certainty=' + query.certainty;
        queryBaseHttp += '&stateCode=' + query.stateCode;
        queryBaseHttp += '&selJson=' + query.selectionJSON;

        queryBaseHttp += '&host=' + db.host;
        queryBaseHttp += '&user=' + db.user;
        queryBaseHttp += '&password=' + db.password;
        queryBaseHttp += '&database=' + db.database;
        queryBaseHttp += '&table=' + db.table;

        return queryBaseHttp;
    }

    function reset() {
        setDB(defhost, defdb, defuser, defpassword, deftable);
        setPointType("");
        setStartEpoch(-1);
        setEndEpoch(-1);
        setFromDuration(-1);
        setToDuration(-1);
        setCertainty(-1);
        setSelection("");
    }

    function clearSelection() {
        query.selectionJSON = "";
    }

    function setSelection(s) {
        query.selectionJSON = s;
    }

    function setDB(host, database, username, password, table) {
        db.host = host;
        db.user = username;
        db.password = password;
        db.table = table;
        db.database = database;
    }

    function setPointType(p) {
        query.pointType = p;
    }

    function setStateCode(s) {
        query.stateCode = s;
    }

    function setStartEpoch(d) {
        query.startEpoch = d;
    }

    function setEndEpoch(d) {
        query.endEpoch = d;
    }

    function setCertainty(c) {
        query.certainty = c;
    }

    function setFromDuration(d) {
        query.fromDuration = d;
    }

    function setToDuration(d) {
        query.toDuration = d;
    }

    return {
        construct : construct,
        reset: reset,
        clearSelection : clearSelection,
        setSelection : setSelection,
        setDB : setDB,
        setPointType : setPointType,
        setStateCode : setStateCode,
        setStartEpoch : setStartEpoch,
        setEndEpoch : setEndEpoch,
        setCertainty : setCertainty,
        setFromDuration : setFromDuration,
        setToDuration : setToDuration
    };
}
